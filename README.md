**This project is a continuation of the [Infrastructure as Code](https://gitlab.com/b.owoyele/infrastructure-as-code) project. Please refer to that project for templates used to launch the VPC infrastructure.**

## **Objectives:**

1. Deploy VPC infrastructure using nested CloudFormation stacks and GitHub Actions.


## **Architecture:**

![CloudFormation](VPC.drawio__1_.png)

## **Requirements**
1. GitHub Repo
2. Nested CloudFormation templates uploaded into an S3 bucket
3. Access to user access key ID and secret acess key
    * **remember security best practices: Do NOT use your root account credential and this user should only be given permissions to perform the desired task.**

## **Solution:**
1. Upload your root stack template ([main.yaml)](https://gitlab.com/b.owoyele/infrastructure-as-code/-/blob/main/main-stack.yaml) to your GitHub Repository. 
2. Add AWS Credentials in the GitHub Repo.
    1. Go to repositroy settings 
    2. On the left hand side under **Security** select **Secrets**
    3. Under **Secrets** select **Actions**
    4. To the right select **New repository secret**
    5. Name your secret **ACCESS_KEY**, insert your access key ID as the value and save. 
    6. Create another secret, name it **ACCESS_KEY_SECRET**, insert your secret access key as the value and save. 
3. Create a workflow file (this is the file that will deploy your root template and essentially your infrastructure).
    1. In your repository go to **Actions**
    2. Select **New worklow** and then select **configure** under **simple workflow**
    3. Edit the file to launch the VPC infrastructure
        * [Sample template](https://gitlab.com/b.owoyele/deploy-vpc-infrastructure-with-cloudformation-and-github-actions/-/blob/main/deploy.yml)
            * this template is configured to trigger the workflow on push and pull requests 
4. Commit the workflow file to the main branch. This will trigger the workflow and launch the CloudFormation stack in AWS. 
5. Upon successfully deploying your infrasturcture you will see the following in your repository under the **Actions** section.
    <details> 
    <summary>Click Here</summary>
      ![Successful launch](https://gitlab.com/b.owoyele/images/-/raw/main/GitHub_Actions.jpeg)

    </details>



